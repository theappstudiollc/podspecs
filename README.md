#PodSpecs for The App Studio LLC

This is a public CocoaPods repository for including [ASEnterpriseFramework](http://bitbucket.org/theappstudiollc/asenterprise.git) into your iOS project.

To use, you must first [install CocoaPods](http://guides.cocoapods.org/using/getting-started.html).

> $ gem install cocoapods --user-install

Then add this to the top of your Podfile

> source 'https://bitbucket.org/theappstudiollc/PodSpecs.git'

An example use inside your Podfile:

> pod 'ASEnterpriseFramework', '1.6.6'

Unlikely, but if the podspec cannot be found, set up a private repository ([more information](http://guides.cocoapods.org/making/private-cocoapods.html))

> $ pod repo add bitbucket-theappstudiollc-podspecs https://bitbucket.org/theappstudiollc/podspecs
