Pod::Spec.new do |s|
s.name = "ASLib"
s.version = "0.9.5"
s.summary = "The App Studio LLCs private library"
s.author = { "DS Mitchell" => "ds.mitchell@theappstudiollc.com" }
s.homepage = "https://bitbucket.org/theappstudiollc/aslib"
s.source = { :git => "https://bitbucket.org/theappstudiollc/aslib", :tag => "0.9.5" }
s.platform = :ios
s.source_files = 'ASLib/*'
s.exclude_files = 'ASLib/*.pch'
s.requires_arc = true
s.prefix_header_contents = %(
	#ifdef __OBJC__
	#import <Foundation/Foundation.h>
	#import "ASPrefixHeader.h"
	#endif)
end